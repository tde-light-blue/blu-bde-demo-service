# Blu BDE Demo Service

Small REST backend service written in JAVA with SpringBoot Reactive that serves for demo and presentation purposes.

## Functionality

The service provides CRUD functionality for a minimal application transformation register.

## Run locally

To run the service locally, don't forget to start the local MariaDB

```bash
cd docker
docker-compose up
```
