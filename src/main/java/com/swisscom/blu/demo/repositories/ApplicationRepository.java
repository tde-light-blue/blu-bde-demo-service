package com.swisscom.blu.demo.repositories;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

import com.swisscom.blu.demo.models.Application;

@Repository
public interface ApplicationRepository extends R2dbcRepository<Application, String> {
}
