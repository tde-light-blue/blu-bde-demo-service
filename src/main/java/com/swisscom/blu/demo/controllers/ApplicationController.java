package com.swisscom.blu.demo.controllers;

import java.net.URI;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.swisscom.blu.demo.models.Application;
import com.swisscom.blu.demo.services.ApplicationService;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/application")
public class ApplicationController {

    private final ApplicationService applicationService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<Application> getAllApplications() {
        final Flux<Application> applicationFlux = applicationService.getAllApplications();
        return applicationFlux;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<ResponseEntity<Application>> getApplicationById(@PathVariable("id") String applicationId) {
        final Mono<Application> applicationMono = applicationService.getApplicationById(applicationId);
        return applicationMono
                .map((Application application) -> ResponseEntity.ok(application))
                .switchIfEmpty(Mono.just(ResponseEntity.notFound().build()));
    }

    @PostMapping()
    public Mono<ResponseEntity<Application>> upsertApplication(@RequestBody Application application) {
        final Mono<Application> createdApplicationMono = applicationService.upsertApplication(application);
        return createdApplicationMono
                .map(createdApplication -> createPostResponseEntity(createdApplication));

    }

    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Void>> deleteApplicationById(@PathVariable("id") String applicationId) {
        final Mono<Void> deletionMono = applicationService.deleteApplicationById(applicationId);
        return deletionMono
                .map(v -> ResponseEntity.status(HttpStatus.NO_CONTENT).build());
    }

    private ResponseEntity<Application> createPostResponseEntity(Application createdApplication) {
        URI uri = UriComponentsBuilder.fromPath("/{id}")
                .buildAndExpand(createdApplication.getApplicationId())
                .toUri();

        return ResponseEntity.created(uri).body(createdApplication);
    }
}
