package com.swisscom.blu.demo.configurations;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class ProxyConfiguration {
	public ProxyConfiguration(@Value("${http.proxy.host:''}") String proxyHost,
			@Value("${http.proxy.port:0}") int proxyPort) {

		log.info("Setup ProxySelector with Proxy Host {} and Port {}", proxyHost, proxyPort);
		initProxySelector(proxyHost, proxyPort);
	}

	private void initProxySelector(String proxyHost, int proxyPort) {
		ProxySelector.setDefault(new ProxySelector() {
			final static List<String> NO_PROXY = Arrays.asList("corproot.net", "swisscom.com", "localhost", "svc.cluster.local");

			@Override
			public List<Proxy> select(URI uri) {
				if (proxyHost.isBlank() || proxyPort == 0) {
					log.debug("No proxy for {}. No proxy configured", uri);
					return List.of(Proxy.NO_PROXY);
				} else if (!uri.toASCIIString().contains("sso-corproot-v2")
						&& NO_PROXY.stream().anyMatch(noproxy -> uri.toString().contains(noproxy))) {
					log.debug("No proxy for {}", uri);
					return List.of(Proxy.NO_PROXY);
				} else {
					log.debug("Proxy for {} ({}:{})", uri, proxyHost, proxyPort);
					return List.of(new Proxy(Type.HTTP, InetSocketAddress.createUnresolved(proxyHost, proxyPort)));
				}
			}

			@Override
			public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
				if (uri == null || sa == null || ioe == null) {
					throw new IllegalArgumentException("Arguments can't be null.");
				}
			}
		});
	}
}
