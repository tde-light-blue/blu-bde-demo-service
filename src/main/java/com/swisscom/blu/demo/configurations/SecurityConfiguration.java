package com.swisscom.blu.demo.configurations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity.OAuth2ResourceServerSpec;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

@EnableWebFluxSecurity
@Configuration
public class SecurityConfiguration {

	private final static List<String> ALLOWED_ORIGIN_PATTERNS = Arrays.asList(
			"http://localhost:*",
			"http://localhost",
			"http://*.svc.cluster.local:*",
			"http://*.svc.cluster.local");

	@Bean
	@Profile("!local")
	public SecurityWebFilterChain filterChain(ServerHttpSecurity http) throws Exception {
		http.authorizeExchange()
				.anyExchange()
				.authenticated()
				.and()
				.oauth2ResourceServer(OAuth2ResourceServerSpec::jwt);

		http.csrf().disable();
		http.cors();

		return http.build();
	}

	@Bean
	@Profile("local")
	public SecurityWebFilterChain filterChainLocal(ServerHttpSecurity http) throws Exception {
		http.authorizeExchange()
				.anyExchange()
				.permitAll();

		http.csrf().disable();
		http.cors();

		return http.build();
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOriginPatterns(ALLOWED_ORIGIN_PATTERNS);
		configuration.setAllowCredentials(true);
		configuration.setAllowedMethods(Collections.singletonList("*"));
		configuration.setAllowedHeaders(Collections.singletonList("*"));
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
}
