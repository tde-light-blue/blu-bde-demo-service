package com.swisscom.blu.demo.services;

import org.springframework.stereotype.Service;

import com.swisscom.blu.demo.models.Application;
import com.swisscom.blu.demo.repositories.ApplicationRepository;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class ApplicationService {

    private final ApplicationRepository applicationRepository;

    public Flux<Application> getAllApplications() {
        return applicationRepository.findAll();
    }

    public Mono<Application> getApplicationById(String applicationId) {
        return applicationRepository.findById(applicationId);
    }

    public Mono<Application> upsertApplication(Application application) {
        return applicationRepository.save(application);
    }

    public Mono<Void> deleteApplicationById(String applicationId) {
        return applicationRepository.deleteById(applicationId);
    }

}
