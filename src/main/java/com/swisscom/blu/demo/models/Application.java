package com.swisscom.blu.demo.models;

import java.util.Objects;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Data;

@Data
@Table("applications")
public class Application implements Persistable<String> {
	@Id
	private String applicationId;
	private String applicationName;
	private String responsiblePerson;
	private String migrationStatus;
	private String responsibleTeamTDE;

	@Override
	public boolean isNew() {
		boolean isNew = Objects.isNull(applicationId);
		if (isNew) {
			this.applicationId = UUID.randomUUID().toString();
		}
		return isNew;
	}

	@Override
	public String getId() {
		return getApplicationId();
	}
}
