DROP TABLE IF EXISTS applications;

CREATE TABLE applications (
  application_id CHAR(40) NOT NULL,
  application_name VARCHAR(50) NOT NULL,
  responsible_person VARCHAR(50) NOT NULL,
  migration_status VARCHAR(50) NOT NULL,
  responsible_team_tde VARCHAR(50) NOT NULL,
  PRIMARY KEY(application_id)
);