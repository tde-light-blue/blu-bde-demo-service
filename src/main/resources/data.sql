INSERT INTO
    applications (
        application_id,
        application_name,
        responsible_person,
        migration_status,
        responsible_team_tde
    )
VALUES
    (
        'e241fecf-1e8d-123a-a297-7ea7dcd5e878',
        'Serine',
        'Diklah Kyllikki',
        'confirmed',
        'Blue'
    ),
    (
        '72844bb9-b9b5-456b-a39e-c2737a53dc34',
        'Storm',
        'Emil Kaneonuskatew',
        'done',
        'Light Blue'
    ),
    (
        'a97db1b7-fd1c-789c-a87c-885a33e161df',
        'Octupus',
        'Venijamin Iustinus',
        'open',
        'Light Red'
    ),
    (
        'd7e72d3f-ec96-444d-a52f-71926881cc13',
        'XTM-RCU',
        'Dayaram Archippus',
        'confirmed',
        'Red'
    ),
    (
        '0ac9658b-b151-888e-9d0c-030818ab435e',
        'Shop VM 3.0',
        'Jenci Ashur',
        'open',
        'Green'
    );